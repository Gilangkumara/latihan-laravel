<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form Sign</title>
</head>
<body>
    <form action="/SignUp" method="POST">
    @csrf
    <h1>Buat Account Baru!</h1>
    <p><h2>Sign Up Form</h2></p>
    <label for="firstname">First name:</label> <br><br>
    <input type="text" name="nama"><br><br>
    
    <label for="lastname">Last name :</label> <br><br>
    <input type="text" name="lastnama"><br><br>
    
    <label for="gender">Gender:</label> <br><br>
        <input type="radio" id="male" name="Gender" value="Male">
            <label for="radio">Male</label><br>
        <input type="radio" id="female" name="Gender" value="Female">
            <label for="radio">Female</label><br>
        <input type="radio" id="other" name="Gender" value="Other">
            <label for="radio">Other</label><br><br>
    
    <label for="nationality">Nationality:</label> <br><br>
    <select name="nationality" id="nationality">
        <option value="indonesia">Indonesia</option>
        <option value="arab">Arab</option>
        <option value="emglish">English</option>
        <option value="other">Other</option>
      </select><br><br>

    <label for="language">Language Spoken:</label><br>
    <p><input type="checkbox" value="bahasa indonesia" name="language">Bahasa Indonesia</p>
    <p><input type="checkbox" value="bahasa engrish" name="language">Bahasa engrish</p>
    <p><input type="checkbox" value="bahasa arab" name="language">Bahasa Arab</p>

    <label for="Bio">Bio:</label><br><br>
    <textarea name="Bio" cols="30" rows="10"></textarea><br><br>
    <button type="submit" value="Sign Up">Sign Up</button>
</form>
</body>
</html>