<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register()
    {
        return view('register');
    }

    public function kirim(Request $request)
    {
        //dd($request->all());
        $name = $request["nama"];
        $lastname = $request["lastnama"];

        return view('welcome', compact('name','lastname'));
    }
    
}
